<?php
/**
 * @file
 */

// Define debuging constant.
define('BPM_DEBUG', FALSE);
// // Temp for testing purpose.
// if((defined('BPM_DEBUG')) && (BPM_DEBUG == TRUE)) {
//   print ' ' . __FUNCTION__ . PHP_EOL;
// }

/**
 * Class for BpmOnline
 * Examples of using:
 * $bpm = new BpmOnline();
 * // Get the whole collection of objects
 * $bpm->collection ('Builder type');
 * // Add an object
 * $bpm->add ('Object_type', array_parameters);
 * // Get the object by GUID
 * $bpm->show ('GUID_object', 'Business_type');
 * // Change to GUID and type
 * $bpm->edit ('GUID_object', 'Object_type', 'array_data');
 * // Delete the object by GUID and type
 * $bpm->delete ('GUID_business', 'Business_type');
 */
class BpmOnlineClient {
  /**
   * Settings
   *
   * @link string address of the BPM Online location.
   * @account string username and password for access.
   * @mailCampAccount array - account data for MailCamp.
   */
  protected $link;
  protected $account;

  /**
   * {@inheritdoc}
   */
  public function __construct($params) {
    if (isset($params['url'])) {
      $this->link = $params['url'];
    }
    else {
      return NULL;
    }

    if (isset($params['user']) && isset($params['password'])) {
      $this->account = $params['user'] . ':' . $params['password'];
    }
    else {
      return NULL;
    }
  }

  /**
   * Obtaining binaries attached to an object
   *
   * @param $id string Object ID
   * @param $object string object name
   * @return string returns a base64 encoded file
   */
  public function files($id, $object) {
    $result = array();
    $files = $this->request('GET', $object . 'FileCollection/?$filter=' . $object . '/Id%20eq%20guid%27' . $id . '%27')->results;
    foreach ($files as $file) {
      $ch = curl_init($file->Data->__mediaresource->edit_media);
      $options = array(
        CURLOPT_CONNECTTIMEOUT => 10,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
          'MaxDataServiceVersion: 3.0',
          'Content-Type: application/json;odata=verbose',
          'DataServiceVersion: 1.0',
          'Authorization: Basic ' . base64_encode($this->account)
        )
      );
      curl_setopt_array($ch, $options);
      $result[] = curl_exec($ch);
      curl_close($ch);
    }
    return $result;
  }

  /**
   * Send cURL request to BPM Online OData
   *
   * @param $method string (GET|POST|PUT|DELETE)
   * @param $link string the link to which the request will be made
   * @param $data string xml data when adding / editing an object
   * @param boolean $return_xml - should request return xml response.
   *
   * @return string xml BPM Online OData in xml format
   */
  private function request($method, $link, $data = NULL, $return_xml = FALSE) {
    $link = $this->link . $link;

    $ch = curl_init($link);
    $options = array(
      CURLOPT_CONNECTTIMEOUT => 10,
      CURLOPT_CUSTOMREQUEST => $method,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_POSTFIELDS => $data,
      CURLOPT_HTTPHEADER => array('Authorization: Basic ' . base64_encode($this->account))
    );
    // If these parameters are set, request will receive response in json format.
    if (!$return_xml) {
      $options[CURLOPT_HTTPHEADER][] = 'MaxDataServiceVersion: 3.0';
      $options[CURLOPT_HTTPHEADER][] = 'Content-Type: application/json;odata=verbose';
      $options[CURLOPT_HTTPHEADER][] = 'DataServiceVersion: 1.0';
      $options[CURLOPT_HTTPHEADER][] = 'Accept: application/json;odata=verbose';
    }
    curl_setopt_array($ch, $options);
    $result = curl_exec($ch);

    // Check if response is ok.
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    // Close cURL connection.
    curl_close($ch);

    // Temp for testing purpose.
    if ((defined('BPM_DEBUG')) && (BPM_DEBUG == TRUE)) {
      watchdog('bpm-test', 'Response from request: <pre> %response </pre>', array('%response' => print_r($result, TRUE)), WATCHDOG_NOTICE, $link = NULL);
    }

    // If not, provide response.
    if ($http_code == 404) {
      return FALSE;
    }

    if (($method != 'PUT') && ($result != FALSE) && (!$return_xml)) {
      return json_decode($result)->d;
    }

    return $result;
  }

  /**
   * Uploading images to BPM Online OData
   *
   * @param $link
   * @param $guid string guid image that loads the binary data
   * @param $file string binary file data
   * @return string
   */
  public function upload($link, $guid, $file) {
    if ($link == 'SysImage') {
      $link = $this->link . urlencode($link . "Collection(guid'$guid')") . '/PreviewData';
    }
    else {
      $link = $this->link . urlencode($link . "Collection(guid'$guid')") . '/Data';
    }
    $ch = curl_init($link);
    $options = array(
      CURLOPT_CUSTOMREQUEST => 'PUT',
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_POSTFIELDS => $file,
      CURLOPT_HTTPHEADER => array(
        'Content-Type: multipart/form-data;boundary=+++++',
        'Authorization: Basic ' . base64_encode($this->account)
      )
    );
    curl_setopt_array($ch, $options);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
  }

  /**
   * A method for obtaining a collection of objects of a certain type, with the ability to specify a filter
   *
   * @param string $object name of the objects to be retrieved
   * @param string $filter object filter
   *
   * @return stdClass|boolean
   */
  public function collection($object, $filter = NULL) {
    $response_from_api = $this->request('GET', $object . "Collection" . $filter);

    if (isset($response_from_api->results)) {
      return $response_from_api->results;
    }

    return FALSE;
//    return $this->request('GET', $object . "Collection" . $filter)->results;
  }

  /**
   * @param $object
   * @param null $filter
   *
   * @return mixed
   */
  protected function filterCollection($object, $filter = NULL) {
    return $this->request('GET', $object . 'Collection/?$filter=' . $filter)->results;
  }

  /**
   * A method for obtaining a collection of objects of a certain type, with the ability to specify a filter
   *
   * @return stdClass
   */
  public function getCollection() {
    return $this->request('GET', '/')->EntitySets;
  }


  /**
   * @todo temp solution to retrieve available fields from API
   *
   * @param string $object name of the objects to be retrieved
   * @param string $filter object filter
   * @return stdClass
   */
  public function getModuleFields($object, $filter = NULL) {
    $response = array();

    if ($filter == NULL) {
      $filter = '/?$top=1';
    }
    else {
      $filter .= '&$top=1';
    }

//    $module = $this->request('GET', $object . $filter)->results;

    $module = $this->request('GET', $object . $filter, NULL, TRUE);
    // Create xml from string.
    $xml = simplexml_load_string($module);

    foreach ($xml->entry->content as $data) {
      $namespaces = $data->getNameSpaces(TRUE);
      $dataXml = $data->children($namespaces['m'])->children($namespaces['d']);

      // Go through each field.
      foreach ($dataXml as $data) {
        // Get field values anf format them.
        $field_data_arr = $this->convertXmlToArray($data);
        $field_name = key($field_data_arr);
        $field_type = isset($field_data_arr[$field_name]['@m:type']) ? $field_data_arr[$field_name]['@m:type'] : 'textfield';
        $field_value = isset($field_data_arr[$field_name]['$']) ? $field_data_arr[$field_name]['$'] : $field_data_arr[$field_name];

        $response[$field_name]['type'] = $field_type;
        $response[$field_name]['name'] = $field_name;
        $response[$field_name]['value'] = $field_value;
      }
    }
    if (!$module) {
      return FALSE;
    }

//    foreach ($module[0] as $k => $a) {
//      if (is_array($a)) {
//        unset($module[0][$k]);
//      }
//    }

//    dpm($response, 'response');
//    dpm(array_keys((array)$module[0]), 'original response');
//    return array_keys((array)$module[0]);
    return $response;
  }

  /**
   * Converts xml to array.
   * This code is take from:
   * https://outlandish.com/blog/tutorial/xml-to-json/
   *
   * @param $xml
   * @param array $options
   *
   * @return array
   */
  public function convertXmlToArray($xml, $options = array()) {
    $defaults = array(
      'namespaceSeparator' => ':',//you may want this to be something other than a colon
      'attributePrefix' => '@',   //to distinguish between attributes and nodes with the same name
      'alwaysArray' => array(),   //array of xml tag names which should always become arrays
      'autoArray' => TRUE,        //only create arrays for tags which appear more than once
      'textContent' => '$',       //key used for the text content of elements
      'autoText' => TRUE,         //skip textContent key if node has no attributes or child nodes
      'keySearch' => FALSE,       //optional search and replace on tag and attribute names
      'keyReplace' => FALSE       //replace values for above search values (as passed to str_replace())
    );
    $options = array_merge($defaults, $options);
    $namespaces = $xml->getDocNamespaces();
    $namespaces[''] = NULL; //add base (empty) namespace

    //get attributes from all namespaces
    $attributesArray = array();
    foreach ($namespaces as $prefix => $namespace) {
      foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
        //replace characters in attribute name
        if ($options['keySearch']) $attributeName =
          str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
        $attributeKey = $options['attributePrefix']
          . ($prefix ? $prefix . $options['namespaceSeparator'] : '')
          . $attributeName;
        $attributesArray[$attributeKey] = (string)$attribute;
      }
    }

    //get child nodes from all namespaces
    $tagsArray = array();
    foreach ($namespaces as $prefix => $namespace) {
      foreach ($xml->children($namespace) as $childXml) {
        //recurse into child nodes
        $childArray = $this->convertXmlToArray($childXml, $options);
        list($childTagName, $childProperties) = each($childArray);

        //replace characters in tag name
        if ($options['keySearch']) $childTagName =
          str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
        //add namespace prefix, if any
        if ($prefix) $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;

        if (!isset($tagsArray[$childTagName])) {
          //only entry with this key
          //test if tags of this type should always be arrays, no matter the element count
          $tagsArray[$childTagName] =
            in_array($childTagName, $options['alwaysArray']) || !$options['autoArray']
              ? array($childProperties) : $childProperties;
        }
        elseif (
          is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName])
          === range(0, count($tagsArray[$childTagName]) - 1)
        ) {
          //key already exists and is integer indexed array
          $tagsArray[$childTagName][] = $childProperties;
        }
        else {
          //key exists so convert to integer indexed array with previous value in position 0
          $tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
        }
      }
    }

    //get text content of node
    $textContentArray = array();
    $plainText = trim((string)$xml);
    if ($plainText !== '') $textContentArray[$options['textContent']] = $plainText;

    //stick it all together
    $propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
      ? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;

    //return node as array
    return array(
      $xml->getName() => $propertiesArray
    );
  }



  /**
   * Method for adding an object, returns the created object
   *
   * @param string $object the name of the object
   * @param array $data array with data to add
   * @return stdClass created object
   */
  public function add($object, array $data) {
    return $this->request('POST', $object, json_encode($data));
  }

  /**
   * Method for obtaining an object by its GUID and type in the BPM Online database
   *
   * @param $id string guid
   * @param $object string object type
   * @return stdClass
   */
  public function show($id, $object) {
    return $this->request('GET', $object . "Collection(guid'$id')");
  }

  /**
   * Method for changing an object by its GUID and type in the BPM Online database
   *
   * @param string $guid object
   * @param string $object the name of the object
   * @param array $data array with data to add
   * @return boolean
   */
  public function edit($guid, $object, array $data) {
    $collection_string_inside = strpos($object, 'Collection');

    if (!$collection_string_inside) {
      return $this->request('PUT', $object . "Collection(guid'$guid')", json_encode($data));
    }

    return $this->request('PUT', $object . "(guid'$guid')", json_encode($data));
  }

  /**
   * Method for deleting an object by its GUID and type in the BPM Online database
   *
   * @param string $guid object
   * @param string $object the name of the object
   */
  public function delete($guid, $object) {
    $this->request('DELETE', $object . 'Collection(' . urlencode("guid'$guid'") . ')');
  }

  /**
   * @param string $operation
   * @param null $name
   *
   * @return array|bool|mixed|void
   */
  public function bpmMailingList($operation = 'get list', $name = NULL) {
    switch ($operation) {
      case 'get list':
        return $this->bpmMailingListGet();
      case 'get modified subscribers':
        return $this->bpmMailingListSubsModifiedGet();
      case 'get list by name':
        return $this->bpmMailingListMailingGroupByName($name);
      case 'get subscription types':
        return $this->getSubscriptionStatus();
      default:
        return FALSE;
    }
  }

  /**
   * Method return data general data about all mailing lists
   * on BPMOnline.
   *
   * @return array
   */
  protected function bpmMailingListGet() {
    $listResponse = array();

    $listAllXml = $this->mailingListGetRequest('BulkEmailType');

    $i = 0;
    foreach ($listAllXml->entry as $list) {
      $listDataArr = $this->extractDataFromXml($list);

      $createdDate = new DateTime($listDataArr['CreatedOn']);
      $modifiedDate = new DateTime($listDataArr['ModifiedOn']);

      $listResponse[$i]['id'] = $listDataArr['Id'];
      $listResponse[$i]['name'] = $listDataArr['Name'];
      $listResponse[$i]['createdOn'] = $createdDate->format('U');
      $listResponse[$i]['modifiedOn'] = $modifiedDate->format('U');

      $i++;
    }

    return $listResponse;
  }


  /**
   * @return array
   */
  public function bpmMailingListSubsModifiedGet() {
    $subscriptionResponse = array();

    $subscriptionAllXml = $this->mailingListGetRequest('BulkEmailSubscription');
    $period = $this->epochTimePeriodGet('all');

    $i = 0;
    foreach ($subscriptionAllXml->entry as $subscription) {
      $subscriptionDataArr = $this->extractDataFromXml($subscription);

      $createdDate = new DateTime($subscriptionDataArr['CreatedOn']);
      $modifiedDate = new DateTime($subscriptionDataArr['ModifiedOn']);

      $subscribedStatusId = $this->getSubscriptionStatus('Subscribed');
      $subscribedStatusId = array_flip($subscribedStatusId);

      if (($period['start'] > $modifiedDate->format('U')) || ($period['end'] < $modifiedDate->format('U'))) {
        continue;
      }

      $subscriptionResponse[$i]['id'] = $subscriptionDataArr['Id'];
      $subscriptionResponse[$i]['createdOn'] = $createdDate->format('d.m.Y. H:m:s');;
      $subscriptionResponse[$i]['modifiedOn'] = $modifiedDate->format('d.m.Y. H:m:s');;
      $subscriptionResponse[$i]['contactId'] = $subscriptionDataArr['ContactId'];
      $subscriptionResponse[$i]['groupId'] = $subscriptionDataArr['BulkEmailTypeId'];
      $subscriptionResponse[$i]['subscriptionStatusId'] = $subscribedStatusId[$subscriptionDataArr['BulkEmailSubsStatusId']];

      $contactObjXml = $this->mailingListGetRequest('Contact', NULL, $subscriptionDataArr['ContactId']);
      $contactObjArr = $this->extractDataFromXml($contactObjXml);

      $subscriptionResponse[$i]['email'] =  $contactObjArr['Email'] ? $contactObjArr['Email'] : t('not available');
      $subscriptionResponse[$i]['name'] = $contactObjArr['Name'] ? $contactObjArr['Name'] : t('not available');
      $subscriptionResponse[$i]['firstname'] = $contactObjArr['GivenName'] ? $contactObjArr['GivenName'] : t('not available');;
      $subscriptionResponse[$i]['lastname'] = $contactObjArr['Surname'] ? $contactObjArr['Surname'] : t('not available');;

      $groupObjXml = $this->mailingListGetRequest('BulkEmailType', NULL, $subscriptionDataArr['BulkEmailTypeId']);
      $groupObjArr = $this->extractDataFromXml($groupObjXml);

      $subscriptionResponse[$i]['Mailgroup name'] = $groupObjArr['Name'];

      $i++;
    }

    return $subscriptionResponse;
  }

  /**
   * Changes status of Subscription to email list.
   *
   * @param $state
   * @param $guid
   * @param $timestamp
   *
   * @return void
   */
  public function updateSubscriptionStatus($state, $guid, $timestamp) {
    $timestamp_c_format = $this->convertTimestampFormat($timestamp);
    $data = array('BulkEmailSubsStatusId' => $state, 'ModifiedOn' => $timestamp_c_format);

    return $this->edit($guid, 'BulkEmailSubscription', $data);
  }


  /**
   * Returns users guid based on email.
   *
   * @param string $email
   * @return string - guid of that bpm contact.
   */
  public function bpmMailingListSubByEmail($email) {
    $filter = 'Email%20eq%20\'' . $email . '\'';
    $contactXml = $this->mailingListGetRequest('Contact', $filter);

    // Check if user with that email exists.
    if (!isset($contactXml->entry)) {
      return 'NA';
    }

    $contact = $this->extractDataFromXml($contactXml->entry);
    return $contact['Id'];
  }

  /**
   * @param $name
   *
   * @return mixed
   */
  protected function bpmMailingListMailingGroupByName($name) {
    $filter = 'Name%20eq%20\'' . $name . '\'';
    $filter = preg_replace('/ /', '%20', $filter);

    $groupXml = $this->mailingListGetRequest('BulkEmailType', $filter);

    $group = $this->extractDataFromXml($groupXml->entry);
    return $group['Id'];
  }

  /**
   * @param $contactGuid
   * @param $groupGuid
   *
   * @return mixed
   */
  protected function bpmMailingListSubscriptionByEmailGroupName($contactGuid, $groupGuid) {
    $subscriptionArrXml = $this->mailingListGetRequest('BulkEmailSubscription');
    foreach ($subscriptionArrXml->entry as $subscriptionXml) {

      $subscription = $this->extractDataFromXml($subscriptionXml);
      if (($subscription['ContactId'] == $contactGuid) && ($subscription['BulkEmailTypeId'] == $groupGuid)) {
        return $subscription['Id'];
      }
    }
  }

  /**
   * Returns list of subscribers to the email list
   *
   * @param [type] $listName
   * @return void
   */
  public function getMailingListSubscribers($paramListName = NULL) {
    // Temp for testing purpose.
    if ((defined('BPM_DEBUG')) && (BPM_DEBUG == TRUE)) {
      print ' ' . __FUNCTION__ . PHP_EOL;
    }

    $listArr = array();
    $subsArr = array();
    $responseArr = array();

    $subStatusArr = $this->getSubscriptionStatus();
    $subsListAllXml = $this->mailingListGetRequest('BulkEmailSubscription');
    $subsListAll = json_decode(json_encode((array)$subsListAllXml), TRUE);

    $i = 0;
    foreach ($subsListAll['entry'] as $list) {
      $listSingle = $this->mailingListGetRequest('BulkEmailSubscription', NULL, NULL, $list['id']);
      $listSingleTemp = $listSingle->content->children('http://schemas.microsoft.com/ado/2007/08/dataservices/metadata')->children("http://schemas.microsoft.com/ado/2007/08/dataservices");
      $arrayTemp = json_decode(json_encode((array)$listSingleTemp), TRUE);

      $listArr[$arrayTemp['BulkEmailTypeId']][$i]['ContactId'] = $arrayTemp['ContactId'];
      $listArr[$arrayTemp['BulkEmailTypeId']][$i]['BulkEmailSubsStatusId'] = $arrayTemp['BulkEmailSubsStatusId'];
      $listArr[$arrayTemp['BulkEmailTypeId']][$i]['SubscriptionId'] = $arrayTemp['Id'];
      $listArr[$arrayTemp['BulkEmailTypeId']][$i]['CreatedOn'] = $arrayTemp['CreatedOn'];
      $listArr[$arrayTemp['BulkEmailTypeId']][$i]['ModifiedOn'] = $arrayTemp['ModifiedOn'];
      $i++;
    }

    foreach ($listArr as $id => $list) {
      $subsSingle = $this->mailingListGetRequest('BulkEmailType', NULL, $id);
      $subsSingleTemp = $subsSingle->content->children('http://schemas.microsoft.com/ado/2007/08/dataservices/metadata')->children("http://schemas.microsoft.com/ado/2007/08/dataservices");
      $arrayTemp = json_decode(json_encode((array)$subsSingleTemp), TRUE);
      if (($paramListName != NULL) && ($paramListName != $arrayTemp['Name'])) {
        continue;
      }
      $listName = $arrayTemp['Name'];
      $i = 0;
      foreach ($list as $contact) {
        if ($contact['BulkEmailSubsStatusId'] != $subStatusArr['Subscribed']) {
          continue;
        }

        $contactSingle = $this->mailingListGetRequest('Contact', NULL, $contact['ContactId']);
        $contactSingleTemp = $contactSingle->content->children('http://schemas.microsoft.com/ado/2007/08/dataservices/metadata')->children("http://schemas.microsoft.com/ado/2007/08/dataservices");
        $arrayTemp = json_decode(json_encode((array)$contactSingleTemp), TRUE);

        if ($arrayTemp['DoNotUseMail'] == 'false') {
          $subStatusArrFlip = array_flip($subStatusArr);
          $subsArr[$listName]['Contacts'][$i]['Id'] = $arrayTemp['Id'];
          $subsArr[$listName]['Contacts'][$i]['Name'] = $arrayTemp['Name'];
          $subsArr[$listName]['Contacts'][$i]['Email'] = $arrayTemp['Email'];
          $subsArr[$listName]['Contacts'][$i]['Status'] = $subStatusArrFlip[$contact['BulkEmailSubsStatusId']];
          $subsArr[$listName]['Contacts'][$i]['SubscriptionId'] = $contact['SubscriptionId'];
          $subsArr[$listName]['Contacts'][$i]['CreatedOn'] = $contact['CreatedOn'];
          $subsArr[$listName]['Contacts'][$i]['ModifiedOn'] = $contact['ModifiedOn'];

          $i++;
        }
      }
    }

    return $subsArr;
  }


  /**
   * Returns all available subscription statuses.
   *
   * @return void
   */
  public function getSubscriptionStatus() {
    // Temp for testing purpose.
    if ((defined('BPM_DEBUG')) && (BPM_DEBUG == TRUE)) {
      print ' ' . __FUNCTION__ . PHP_EOL;
    }

    $statusArr = array();

    $statusXML = $this->mailingListGetRequest('BulkEmailSubsStatus');

    foreach ($statusXML->entry as $statusSingle) {
      $statusSingleTemp = $statusSingle->content->children('http://schemas.microsoft.com/ado/2007/08/dataservices/metadata')->children("http://schemas.microsoft.com/ado/2007/08/dataservices");
      $arrayTemp = json_decode(json_encode((array)$statusSingleTemp), TRUE);
      $statusArr[$arrayTemp['Name']] = $arrayTemp['Id'];
    }

    return $statusArr;
  }

  /**
   * Performs curl request for mailing lists.
   *
   * @param [type] $entity
   * @param [type] $filter
   * @param [type] $guid
   * @param [type] $guidUrl
   * @return void
   */
  protected function mailingListGetRequest($entity, $filter = NULL, $guid = NULL, $guidUrl = NULL) {
    // Temp for testing purpose.
    if ((defined('BPM_DEBUG')) && (BPM_DEBUG == TRUE)) {
      print ' ' . __FUNCTION__ . PHP_EOL;
    }

    $url = $this->link . $entity . 'Collection';
    if ($guid != NULL) {
      $url .= "(guid'" . $guid . "')";
    }
    elseif ($guidUrl != NULL) {
      $url = $guidUrl;
    }

    if ($filter != NULL) {
      $url .= '/?$filter=' . $filter;
    }

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'MaxDataServiceVersion: 3.0',
      'Content-Type: application/xml',
      'DataServiceVersion: 1.0',
      'Authorization: Basic ' . base64_encode($this->account)));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
    $data = curl_exec($ch);
    curl_close($ch);

    return new SimpleXMLElement($data);
  }



  /**
   * Performs curl request for mailing lists.
   *
   * @param [type] $entity
   * @param [type] $filter
   * @param [type] $guid
   * @param [type] $guidUrl
   * @return void
   */
  public function bpmApiRequest($entity, $filter = NULL, $guid = NULL, $guidUrl = NULL) {
    // Temp for testing purpose.
    if ((defined('BPM_DEBUG')) && (BPM_DEBUG == TRUE)) {
      print ' ' . __FUNCTION__ . PHP_EOL;
    }

    // Set initial data for url.
    $url = $this->link . $entity . 'Collection';

    // If request to certain object based on guid is what is wanted.
    if ($guid != NULL) {
      $url .= "(guid'" . $guid . "')";
    }
    // If url for request is passed.
    elseif ($guidUrl != NULL) {
      $url = $guidUrl;
    }

    // Set filter, if value for filter is passed.
    if ($filter != NULL) {
      $url .= '/?$filter=' . $filter;
    }

    // Perform batch request.
    // Because there is a lot of data for Contacts and Subscriptions, data for them
    // will be downloaded in batches of 200. If there are $guid and $guidUrl passed as
    // parameters, there is no need to get this data in batches, that is a request
    // for a single piece of data.
    if ((($entity == 'Contact') || ($entity == 'BulkEmailSubscription')) &&
      ($guid == NULL) && ($guidUrl == NULL)) {
      // Set that 200 pieces of data are get per batch.
      $top = 200;
      $skip = 0;
      $filter_exist = FALSE;

      // To properly format url, check if any of the filters is applied.
      if ($filter != NULL) {
        $filter_exist = $filter;
      }

      do {
        // Method is created specially for send requests in batches.
        $responseXml = $this->batchBpmApiRequest($entity, $top, $skip, $filter_exist);
        // 'Pagination' for request.
        $skip += $top;

        // Format for contact data.
        if ($entity == 'Contact') {
          // Convert data from xml into php array format. If there is no data from API,
          // there will be no 'entry' child and that would be the signal to get out of the loop.
          foreach ($responseXml->entry as $contact) {

            // Convert and format data with this method
            $responseArr[] = $this->formatContactData($contact);
          }
        }

        // Format for BulkEmailSubscription data.
        if ($entity == 'BulkEmailSubscription') {

          // If there are no data.
          if ((!isset($responseXml->entry)) && (empty($responseArr))) {
            return FALSE;
          }

          // Going through each subscription.
          foreach ($responseXml->entry as $sub) {

            // Extracting part of the data from XML and converting it into php array.
            $subDecoded = json_decode(json_encode((array)$sub), TRUE);

            // Extracting rest of the needed data and converting it from XML into php array.
            $dataXML = $sub->content->children('http://schemas.microsoft.com/ado/2007/08/dataservices/metadata')->children("http://schemas.microsoft.com/ado/2007/08/dataservices");
            $dataArr = json_decode(json_encode((array)$dataXML), TRUE);
            // Merging two sets of data.
            $dataArr['request_timestamp'] = $subDecoded['updated'];
            // Adding individual sub data into array for response.
            $responseArr[] = $dataArr;
          }
        }
      } while (isset($responseXml->entry));

      // Return data.
      return $responseArr;
    }

    // Perform regular request.
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'MaxDataServiceVersion: 3.0',
      'Content-Type: application/xml',
      'DataServiceVersion: 1.0',
      'Authorization: Basic ' . base64_encode($this->account)));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
    $data = curl_exec($ch);
    curl_close($ch);

    return new SimpleXMLElement($data);
  }

  /**
   * Performs batched request for situations when we expect massive amount of data
   * as response from API.
   * @param string $entity - name of data object.
   * @param integer $top - amount of data to get in one batch.
   * @param integer $skip - how much we have already downloaded and we need to skip it.
   * @param bool $filter - are there any filters applied, because of formatting url of the request.
   *
   * @return bool|\SimpleXMLElement object.
   */
  public function batchBpmApiRequest($entity, $top, $skip, $filter = FALSE) {
    // Temp for testing purpose.
    if ((defined('BPM_DEBUG')) && (BPM_DEBUG == TRUE)) {
      print ' ' . __FUNCTION__ . PHP_EOL;
    }

    // Set initial data for url.
    $url = $this->link . $entity . 'Collection';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'MaxDataServiceVersion: 3.0',
      'Content-Type: application/xml',
      'DataServiceVersion: 1.0',
      'Authorization: Basic ' . base64_encode($this->account)));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);

    // Fix to receive all objects from response.
    if (!$filter) {
      // Data is sorted by creation timestamp.
      $url .= '/?$top=' . $top . '&$skip=' . $skip . '&$orderby=CreatedOn';
    }
    else {
      // Data is sorted by creation timestamp.
      $url .= '/?$filter=' . $filter . '&$top=' . $top . '&$skip=' . $skip . '&$orderby=CreatedOn';
    }

    curl_setopt($ch, CURLOPT_URL, $url);
    $data = curl_exec($ch);
    curl_close($ch);

    // Response.
    return new SimpleXMLElement($data);
  }

  /**
   * Autodetect $timestamp format, converts it from epoch time to 'c' format and
   * vice versa.
   *
   * @param string $timestamp
   *
   * @return string
   */
  public function convertTimestampFormat($timestamp) {

    // Converts date from epoch time ('U') to ISO 8601 ('c') datetime format.
    if (DateTime::createFromFormat('U', $timestamp) !== FALSE) {
      $local_timestamp = DateTime::createFromFormat('U', $timestamp);

      // Return ISO 8601 date format.
      return $local_timestamp->format('c');
    }

    // Converts date from ISO 8601 ('c') to epoch time ('U') datetime format.
    try {
      $local_timestamp = new DateTime($timestamp);

      return $local_timestamp->format('U');
    }
    // Catch exception so its not displayed to the user.
    catch (exception $e) {

    }

    return FALSE;
  }

  /**
   * Converts XML object to array format.
   *
   * @param object $xml
   * @return array
   */
  protected function xmlToArray($xml) {
    return json_decode(json_encode((array)$xml), TRUE);
  }

  /**
   * Extracts data with specific attributes from
   * xml object.
   *
   * @param object $dataOriginal - part of the XML object.
   * @return array
   */
  protected function extractDataFromXml($dataOriginal) {
    $dataXml = $dataOriginal->content->children('http://schemas.microsoft.com/ado/2007/08/dataservices/metadata')->children('http://schemas.microsoft.com/ado/2007/08/dataservices');

    return $this->xmlToArray($dataXml);
  }

  /**
   * Gets mailing list data from BPM API.
   *
   * @param string $name - optional name of the list to filter data.
   * @return array $listResponse
   */
  public function getListId($name = NULL) {
    // Temp for testing purpose.
    if ((defined('BPM_DEBUG')) && (BPM_DEBUG == TRUE)) {
      print ' ' . __FUNCTION__ . PHP_EOL;
    }

    // Variable init.
    $filter = NULL;
    $list_response = array();

    // Setting up filter based on parameter.
    if ($name != NULL) {
      $name = urlencode($name);
      $filter = 'Name%20eq%20\'' . $name . '\'';
    }

    // Getting all the available lists on the BPM API.
    $list_all_xml = $this->bpmApiRequest('BulkEmailType', $filter);

    // There maybe multiple lists with same name, so we are looping if there are multiple lists.
    $i = 0;
    foreach ($list_all_xml->entry as $list) {
      // Extract data from XML and convert it to php array.
      $list_data_arr = $this->extractDataFromXml($list);

      // Extracting only necessary from newsletter list data.
      $list_response[$i]['id'] = $list_data_arr['Id'];
      $list_response[$i]['name'] = $list_data_arr['Name'];
      $list_response[$i]['createdOn'] = $list_data_arr['CreatedOn'];
      $list_response[$i]['modifiedOn'] = $list_data_arr['ModifiedOn'];
      $list_response[$i]['createdOnEpoch'] = date('U', strtotime($list_data_arr['CreatedOn']));
      $list_response[$i]['modifiedOnEpoch'] = date('U', strtotime($list_data_arr['ModifiedOn']));
      $i++;
    }
    if (empty($list_response)) {
      return 'No list under that name';
    }

    return $list_response;
  }

  /**
   * Gets available subscription states from BPM API.
   *
   * @param boolean $associative - option if response should be in a form of an associative array.
   * @return array $statusArr
   */
  public function getStates($associative = FALSE) {
    // Temp for testing purpose.
    if ((defined('BPM_DEBUG')) && (BPM_DEBUG == TRUE)) {
      print ' ' . __FUNCTION__ . PHP_EOL;
    }

    // Variable init.
    $statusArr = array();
    // Getting all subscriptions states.
    $statusXML = $this->bpmApiRequest('BulkEmailSubsStatus');

    // Extracting only necessary data.
    $i = 0;
    foreach ($statusXML->entry as $statusSingle) {
      // Extracting data from XML and converting them into php array.
      $statusSingleTemp = $statusSingle->content->children('http://schemas.microsoft.com/ado/2007/08/dataservices/metadata')->children("http://schemas.microsoft.com/ado/2007/08/dataservices");
      $arrayTemp = json_decode(json_encode((array)$statusSingleTemp), TRUE);

      // Create associative array as response, based on parameter.
      if ($associative) {
        $statusArr[$arrayTemp['Id']] = $arrayTemp['Name'];
      }
      // Numeric array as response, based on parameter.
      else {
        $statusArr[$i]['id'] = $arrayTemp['Id'];
        $statusArr[$i]['name'] = $arrayTemp['Name'];
        $i++;
      }
    }

    return $statusArr;
  }

  /**
   * Gets subscriptions data from BPM API.
   *
   * @param $timestamp - timestamp when the last sync with MailCamp was performed.
   *
   * @return array $subscriptionsArr
   */
  public function getBPMSubsData($timestamp = NULL) {
    // Temp for testing purpose.
    if ((defined('BPM_DEBUG')) && (BPM_DEBUG == TRUE)) {
      print ' ' . __FUNCTION__ . PHP_EOL;
    }

    // Variable init.
    $subscriptionsArr = array();

    // If needed, create filter.
    $filter = NULL;
    if ($timestamp != NULL) {
      $timestamp_c_format = $this->convertTimestampFormat($timestamp);
      $filter = "CreatedOn%20ge%20datetime'" . urlencode($timestamp_c_format) . "'";
    }

    // Get all mailing lists subscriptions.
    $subscriptionsArr = $this->bpmApiRequest('BulkEmailSubscription', $filter);

    return $subscriptionsArr;
  }

  /**
   * Gets BPM Contact data.
   * If there is no value for $contactId, all available data for contacts will be returned.
   *
   * @param string $contactId - optional parameter (guid) to filter contacts.
   *
   * @return array
   */
  public function getContact($contactId = NULL) {
    // Temp for testing purpose.
    if ((defined('BPM_DEBUG')) && (BPM_DEBUG == TRUE)) {
      print ' ' . __FUNCTION__ . PHP_EOL;
    }

    // Variable init.
    $responseArr = array();

    // Getting contacts, applying filter if needed.
    $contactsXml = $this->bpmApiRequest('Contact', NULL, $contactId);

    // All contacts are requested.
    if ($contactId == NULL) {
      return $contactsXml;
    }

    // Request for individual contact, this requires different formatting,
    // extract XML data from 'content'.
    if (!isset($contactsXml->entry)) {
      $responseArr[] = $this->formatContactData($contactsXml);
    }

    // Response.
    return $responseArr;
  }

  /**
   * Gets subscription to newsletter data from BPM.
   *
   * @param string $listName - optional name of the list to filter data.
   * @param string $stateId - BPM ID of the state of the subscription.
   * @param integer $timestamp - timestamp when last sync with MC was performed.
   * @param boolean $createQueue
   *
   * @return array
   */
  public function getSubscriptions($listName = NULL, $stateId = NULL, $timestamp = NULL, $createQueue = FALSE) {
    // Temp for testing purpose.
    if ((defined('BPM_DEBUG')) && (BPM_DEBUG == TRUE)) {
      print ' ' . __FUNCTION__ . PHP_EOL;
    }

    // Variable init.
    $responseArr = array();

    // Get all necessary data for that mailing list and state.
    $listsArr = $this->getListId($listName);
    $statesArr = $this->getStates(TRUE);
    $subsDataArr = $this->getBPMSubsData($timestamp);
    $contactsArr = $this->getContact();

    // Get data of lists that are selected for sync.
    $selectedListsArr = variable_get('bpm_mailcamp_integration_lists_selected_sync', FALSE);

    // If there are no subscription created after last sync.
    if (!$subsDataArr) {
      return FALSE;
    }

    // Formatting data for response.
    foreach ($subsDataArr as $sub) {
      foreach ($listsArr as $list) {
        if (($sub['BulkEmailTypeId'] == $list['id']) && (($stateId == NULL) || ($sub['BulkEmailSubsStatusId'] == $stateId))) {
          // Removing unnecessary data.
          unset($sub['CreatedById']);
          unset($sub['ModifiedById']);
          unset($sub['ProcessListeners']);

          // Formatting data for response.
          $tempDataArr = $sub;
          $tempDataArr['list_name'] = $list['name'];
          $tempDataArr['state_name'] = $statesArr[$sub['BulkEmailSubsStatusId']];

          // Getting contact data.
          // $contactData = $this->getContact($sub['ContactId']);
          $contactData = $this->filterContacts($sub['ContactId'], $contactsArr);

          // Merge data for this single subscription.
          $currentSubscriberData = array_merge($tempDataArr, $contactData);

          // If the create queue option is set, add this subscription to queue.
          // Also, at least one list must be selected for sync, therefore second condition.
          if (($createQueue == TRUE) && ($selectedListsArr != FALSE)) {
            $this->addToSubscriptionsDrupalQueue($selectedListsArr, $currentSubscriberData);
          }

          // Merging contact data with the rest of the data.
          $responseArr[] = $currentSubscriberData;
        }
      }
    }

    return $responseArr;
  }

  /**
   * Method which gets all modules available on BPM API.
   * @todo Maybe find a way how to filter them out.
   *
   * @return object
   */
  public function getModules() {
    // Variable init.
    $response = array();

    $modules_arr = $this->request('GET', NULL)->EntitySets;

    if (!$modules_arr) {
      return FALSE;
    }

    foreach ($modules_arr as $module) {
      $response[$module] = $module;
    }

    return $response;
  }

  /**
   * Creates Drupal API Queue object with subscription data for lists that are
   * selected for sync.
   *
   * @param array $list_settings
   * @param array $bpm_subscription
   *
   * @return object
   */
  public function addToSubscriptionsDrupalQueue($list_settings, $bpm_subscription) {

      foreach ($list_settings as $list) {

        // Compare mailing list guid. Continue if they do not match.
        if ($bpm_subscription['BulkEmailTypeId'] != $list['bpm_guid']) {
          continue;
        }

        // For comparing purpose, convert timestamp of modification of bpm subscription.
        $modified_on = $this->convertTimestampFormat($bpm_subscription['ModifiedOn']);

        // If the list has set timestamp, check when this subscription is modified.
        if (($list['last_sync'] != NULL) && ($modified_on > $list['last_sync'])) {
          // Create queue object, add to queue.
          $queue = DrupalQueue::get('bpm_subscriptions');
          $queue->createItem($bpm_subscription);

        }
        // Sync was never performed, so add this item to queue.
        elseif ($list['last_sync'] == NULL) {
          // Create queue object, add to queue.
          $queue = DrupalQueue::get('bpm_subscriptions');
          $queue->createItem($bpm_subscription);        }
      }
  }

  /**
   * Custom function.
   * Filters out all contacts array to find user by id
   *
   * @param guid $contact_id - guid of the user
   * @param array $contacts_arr - array of all user
   *
   * @return array
   */
  public function filterContacts($contact_id, $contacts_arr) {
    // Temp for testing purpose.
    if ((defined('BPM_DEBUG')) && (BPM_DEBUG == TRUE)) {
      print ' ' . __FUNCTION__ . PHP_EOL;
    }

    // Search multidimensional array.
    $key = array_search($contact_id, array_column($contacts_arr, 'contact_id'));

    // Response.
    return $contacts_arr[$key];
  }

  /**
   * Formats contact data from BPM API into PHP array.
   *
   * @param xml $contact - XML data about contact
   * @return array
   */
  public function formatContactData($contact) {
    // Temp for testing purpose.
    if ((defined('BPM_DEBUG')) && (BPM_DEBUG == TRUE)) {
      print ' ' . __FUNCTION__ . PHP_EOL;
    }

    $contactXml = $contact->content->children('http://schemas.microsoft.com/ado/2007/08/dataservices/metadata')->children("http://schemas.microsoft.com/ado/2007/08/dataservices");

    $contactArr = json_decode(json_encode((array)$contactXml), TRUE);
    // Formating contact response with only needed data.

    $responseDataArr['contact_id'] = $contactArr['Id'];
    $responseDataArr['contact_name'] = $contactArr['Name'];
    $responseDataArr['contact_createdOn'] = $contactArr['CreatedOn'];
    $responseDataArr['contact_email'] = $contactArr['Email'];
    $responseDataArr['contact_given_name'] = $contactArr['GivenName'];
    $responseDataArr['contact_surname'] = $contactArr['Surname'];
    $responseDataArr['contact_do_not_use_email'] = $contactArr['DoNotUseEmail'];

    return $responseDataArr;
  }

  /**
   * Returns array of subscribers that that has changed from subscribed to unsubscribed since
   * last time sync was performed.
   *
   * @param $list_data - optional filter
   *
   * @return array
   */
  public function getModifiedSubscriptions($list_data = NULL) {
    // Temp for testing purpose.
    if ((defined('BPM_DEBUG')) && (BPM_DEBUG == TRUE)) {
      print ' ' . __FUNCTION__ . PHP_EOL;
    }

    // Response variable init.
    $response_arr = array();

    // Get timestamp when sync was performed last time.
    $last_sync_timestamp_epoch = $list_data['last_sync'];

    // If sync has never been performed, that means that there are no value in MC,
    // so full sync from BPM to MailCamp is needed to be performed, there are no values
    // on MailCamp that need to be updated.
    if ($last_sync_timestamp_epoch == NULL) {
      return FALSE;
    }

    // Convert timestamp from epoch to c format.
    $last_sync_timestamp_c_format = $this->convertTimestampFormat($last_sync_timestamp_epoch);

    // Create filter with timestamp when last time sync was performed.
    $filter = "ModifiedOn%20ge%20datetime'" . urlencode($last_sync_timestamp_c_format) . "'";

    // Get all subscription that are modified since the last sync was performed.
    $subscriptions = $this->bpmApiRequest('BulkEmailSubscription', $filter, NULL);

    // If there are no modified subscribers on BPM.
    if ($subscriptions == FALSE) {
      return FALSE;
    }

    // Go through each subscription and extract data that is needed for response.
    foreach ($subscriptions as $subscription) {
      // Get state guid value for unsubscribed and subscribe from module mapping.
      $subscribe_guid = variable_get('bpm_mailcamp_integration_subscribed', NULL);
      $unsubscribe_guid = variable_get('bpm_mailcamp_integration_unsubscribed', NULL);

      // Remove data that is not needed.
      unset($subscription['ProcessListeners']);

      $subscriber_data = $this->getContact($subscription['ContactId']);
      $mailinglist_data_xml = $this->show($subscription['BulkEmailTypeId'], 'BulkEmailType');

      // Convert from XML to php array.
      $raw_decoded_mailing_list = json_decode(json_encode((array)$mailinglist_data_xml), TRUE);

      // If filter is set, filter out results.
      if (($list_data != NULL) && ($raw_decoded_mailing_list['Name'] != $list_data['name'])) {
        continue;
      }

      $response_temp_preformated['email'] = $subscriber_data['contact_email'];
      $response_temp_preformated['modified_on'] = $this->convertTimestampFormat(
        $subscription['ModifiedOn']);
      $response_temp_preformated['contact_name'] = $subscriber_data['contact_name'];
      $response_temp_preformated['mailinglist_name'] = $raw_decoded_mailing_list['Name'];
      $response_temp_preformated['firstname'] = $subscriber_data['contact_given_name'];
      $response_temp_preformated['lastname'] = $subscriber_data['contact_surname'];

      // Unsubscribed status.
      if ($subscription['BulkEmailSubsStatusId'] == $unsubscribe_guid) {
        $response_temp_preformated['subscription_status'] = 'unsubscribed';
      }
      // Subscribed status.
      // @todo Check if this is really needed.
      elseif ($subscription['BulkEmailSubsStatusId'] == $subscribe_guid) {
        $response_temp_preformated['subscription_status'] = 'subscribed';
      }
      // This part should never be reached, this is a just a precaution.
      else {
        continue;
      }

      $response_arr[] = $response_temp_preformated;
    }

    // Response.
    return $response_arr;
  }

  /**
   * Checks if exists stored data in Queue API, based on $name parameter. If exist, it
   * will return request data. If it doesn't, it will
   *
   * @param string $name - name of the variable in Queue API.
   * @param boolean $array_type - if response should be in form of php array (TRUE) or
   * Queue object (FALSE).
   *
   * @return boolean|array
   */
  protected function getDataQueue($name, $array_type = FALSE) {
    // Temp for testing purpose.
    if ((defined('BPM_DEBUG')) && (BPM_DEBUG == TRUE)) {
      print ' ' . __FUNCTION__ . PHP_EOL;
    }

    // Get data from Queue.
    $queue = DrupalQueue::get($name);

    // If there are no items in Queue for this name, return FALSE.
    if ($queue->numberOfItems() == 0) {
      return FALSE;
    }

    // Check if response should be in form of array or queue object.
    if ($array_type) {
      // Variable init.
      $array_response = array();

      // While there are items in queue.
      while ($item = $queue->claimItem()) {
        $array_response[] = $item->data;
      }

      // Reset that item(s) have been used so they can be used again. Removing items
      // will be done on the end of the sync, with removeDataQueue method.
      db_update('queue')
        ->fields(array('expire' => 0))
        ->condition('name', $name)
        ->execute();

      return $array_response;
    }

    // Default response.
    return $queue;
  }

  /**
   * Insert data from $dataArr into Queue.
   *
   * @param $queueName - name of dataset in Queue.
   * @param $dataArr - array with data to be inserted into queue.
   *
   * @return object
   */
  protected function setDataQueue($queueName, $dataArr) {
    // Temp for testing purpose.
    if ((defined('BPM_DEBUG')) && (BPM_DEBUG == TRUE)) {
      print ' ' . __FUNCTION__ . PHP_EOL;
    }

    // Get instance of Queue object.
    $queue = DrupalQueue::get($queueName);

    // Insert data into queue.
    foreach ($dataArr as $data) {
      $queue->createItem($data);
    }

    // Response.
    return $queue;
  }

  /**
   * Removes data from the Queue API.
   * @param string $queueName - Name of the dataset that needs to be removed
   * from the Queue API.
   *
   * @return bool
   */
  public function removeDataQueue($queueName) {
    // Temp for testing purpose.
    if ((defined('BPM_DEBUG')) && (BPM_DEBUG == TRUE)) {
      print ' ' . __FUNCTION__ . PHP_EOL;
    }

    // @todo Check if this may be done with db query.
    // Get data from Queue.
    $queue = DrupalQueue::get($queueName);

    // While there are items in queue.
    while ($item = $queue->claimItem()) {
      $queue->deleteItem($item);
    }

    // Check if there are any items left in queue for this name.
    if ($queue->numberOfItems() == 0) {
      // If there are no items left in the queue, removal of items was performed
      // successfully.
      return TRUE;
    }
    else {
      // There are items left in the queue, so removal was unsuccessful.
      return FALSE;
    }
  }
}