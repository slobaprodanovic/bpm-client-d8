<?php
/**
 * @file
 */

/**
 * Implements hook_form().
 * Set parameters for module.
 *
 * @param array $form
 * @param array $form_state
 * @return array
 */
function bpm_settings_form($form, &$form_state) {
  // Variable init.
  $form = array();

  // Get current settings variables.
  $values = variable_get('bpm_variables', array());

  // BPM data.
  $form['bpm_online_profile'] = array(
    '#type' => 'fieldset',
    '#title' => t('BPMOnline profile'),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
  );
  $form['bpm_online_profile']['bpm_online_user'] = array(
    '#size' => 15,
    '#type' => 'textfield',
    '#title' => t('User'),
    '#required' => TRUE,
    '#maxlength' => 64,
    '#description' => t('Your username from BPM Online.'),
    '#default_value' => isset($values['bpm_online_user']) ? $values['bpm_online_user'] : '',
  );
  $form['bpm_online_profile']['bpm_online_pass'] = array(
    '#size' => 15,
    '#type' => 'password',
    '#title' => t('Password'),
    '#required' => TRUE,
    '#maxlength' => 64,
    '#attributes' => array('value' => 'xxxxxx'),
    '#description' => t('Your password from BPM Online.'),
  );
  $form['bpm_online_profile']['bpm_online_auth_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#required' => TRUE,
    '#description' => t('BPM Online Restful URL. For example, [BPMonline application address]/0/ServiceModel/EntityDataService.svc/'),
    '#default_value' => isset($values['bpm_online_auth_url']) ? $values['bpm_online_auth_url'] : '',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    // @ignore style_button_submit
    '#value' => t('Submit'),
    '#submit' => array('bpm_settings_form_submit'),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 * Validate settings form data.
 *
 * @param array $form
 * @param array $form_state
 * @return void
 */
function bpm_settings_form_validate($form, &$form_state) {
  // Fix for not displaying password on form and for not changing it
  // if user changes only some other field on the form.
  // Get current password value from database.
  $current_password = isset(variable_get('bpm_variables')['bpm_online_pass']) ? variable_get('bpm_variables')['bpm_online_pass'] : '';
  // Password has not changed, set it to old value from database.
  // 'xxxxxx' is password value that is displayed on the form.
  if ($form_state['values']['bpm_online_pass'] == 'xxxxxx') {
    $form_state['values']['bpm_online_pass'] = $current_password;
  }
}

/**
 * Implements hook_form_submit().
 * Submit fn for the BPM Online admin settings form.
 * @param array $form
 * @param array $form_state
 */
function bpm_settings_form_submit($form, &$form_state) {
  $values = isset($form_state['values']) ? $form_state['values'] : array();
  variable_set('bpm_variables', $values);

  drupal_set_message(t('Settings saved'));
}